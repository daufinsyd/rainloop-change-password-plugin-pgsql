Rainloop change password plugin for PostgreSQL
==============================================

This is a fork of the mysql plugin from [sandroz](https://github.com/sandrodz/rainloop-change-password-plugin-mysql).

Rainloop is one of the best looking web clients. You can download it [here](http://rainloop.net/).

This plugin adds change password capability (for users) to Rainloop webmail if you configure your database with PostgreSQL.

##### Installation is simple:

1. Drop the change-password-pgsql in the plugins directory ;
2. In rainloop admin panel go to Plugins, and activate change-password-pgsql ;
3. Enter postgresql details on the plugin config screen (admin account).

Users can change their password from the settings / chane password menu.
